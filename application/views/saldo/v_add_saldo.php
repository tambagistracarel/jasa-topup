<div class="content-wrapper">
    <div class="content-header">
    </div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-body">
                        <form id="form-tambah-transaksi" method="post" action="<?= site_url('Saldo/proses_simpan') ?>" role="form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control form-control-sm" id="nama" name="nama" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Saldo</label>
                                        <input type="text" class="form-control form-control-sm" id="saldo" name="saldo" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <button id="btn-save-transaksi" type="button" class="btn btn-success"><i class="fas fa-file-export"></i>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function() {
        $("#btn-save-transaksi").on("click", function() {
            let validate = $("#form-tambah-transaksi");
            if (validate) {
                $("#form-tambah-transaksi").submit();
            }
        });
    });
</script>
