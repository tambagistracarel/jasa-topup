<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark"><?= $header ?></h1>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<section class="content">
		<div class="card card-info">
			<div class="card-footer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="<?= site_url(array("Saldo", "tambah")) ?>" class="btn btn-success "><i
							class="fas fa-folder-plus"></i>
					Tambah Saldo
				</a> 
				<div class="card-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="text-align:center">No.</th>
								<th style="text-align:center">Nama</th>
								<th style="text-align:center">Saldo</th>
								<th style="text-align:center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($saldos as $a) {
							?>
								<tr>
									<td style="text-align:center"><?= $no++ ?></td>
									<td style="text-align:center"><?= $a->nama ?></td>
									<td style="text-align:center"><?= $a->saldo ?></td>
									<td style="text-align:center">
										<a href="<?= site_url('Saldo/update/') . $a->id_saldo ?>" class="btn btn-sm btn-primary" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
										<a href="<?= site_url('Saldo/proses_hapus/') . $a->id_saldo ?>" data-id="<?= $a->id_saldo ?>" id="delete_id"
								   class="btn btn-sm btn-danger tombolHapus">
									<fas class="fas fa-trash"></fas>
								</a>
									</td>
								</tr>
							<?php

							}
							?>
						</tbody>
					</table>
				</div>
			</div>
	</section>
</div>
<script>
	$(function() {
		$(".tombolHapus").on("click", function() {
			var id = $(this).data('id');
			SwalDelete(id);
		});
	});

	function SwalDelete(id) {
		Swal.fire({
			title: ' Hapus Data Transaksi Ini?',
			text: " ",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#20B2AA',
			cancelButtonColor: '#FF7F00',
			confirmButtonText: 'Hapus Data ',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve) {
					var url = "saldo/proses_hapus/"
					$.ajax({
						url: '<?= base_url() ?>' + url + id,
						type: "POST",
					})
							.done(function(id) {
								window.location.replace("<?= site_url("Transaksi") ?>");
								Swal.fire('Hapus Data Berhasil', 'Data Anda Telah Terhapus!', 'success')
							})
							.fail(function() {
								Swal.fire('Maaf', 'Data Anda Sudah Masuk proses Transaksi', 'error')
							});
				});
			},
		});
	}
</script>
