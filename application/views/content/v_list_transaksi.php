<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark"><?= $header ?></h1>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<section class="content">
		<div class="card card-info">
			<div class="card-footer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="<?= site_url(array("Transaksi", "tambah")) ?>" class="btn btn-success "><i
							class="fas fa-folder-plus"></i>
					Tambah Transaksi
				</a> 
				<div class="card-body">
					<table id="table-item" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="text-align:center">No.</th>
								<th style="text-align:center">Tanggal</th>
								<th style="text-align:center">Jenis Topup</th>
								<th style="text-align:center">No. Pelanggan</th>
								<th style="text-align:center">Harga Pokok</th>
								<th style="text-align:center">Harga Jual</th>
								<th style="text-align:center">Laba</th>
								<th style="text-align:center">Cicilan</th>
								<th style="text-align:center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($transaksis as $a) {
							?>
								<tr>
									<td style="text-align:center"><?= $no++ ?></td>
									<td style="text-align:center"><?= $a->tanggal ?></td>
									<td style="text-align:center"><?= $a->jenis ?></td>
									<td style="text-align:center"><?= $a->no_cust ?></td>
									<td style="text-align:center"><?= $a->hrg_pokok ?></td>
									<td style="text-align:center" id="laba"><?= $a->hrg_jual ?></td>
									<td style="text-align:center"><?= $a->laba ?></td>
									<td style="text-align:center"><?= $a->lunas_blmlns ?></td>
									<td style="text-align:center">
										<a href="<?= site_url('Transaksi/update/') . $a->id_transaksi ?>" class="btn btn-sm btn-primary" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
										<a href="<?= site_url('Transaksi/proses_hapus/') . $a->id_transaksi ?>" data-id="<?= $a->id_transaksi ?>" id="delete_id"
								   class="btn btn-sm btn-danger tombolHapus">
									<fas class="fas fa-trash"></fas>
								</a>
									</td>
								</tr>
							<?php

							}
							?>
						</tbody>
					</table>
				</div>
						<h3><span id="jumlah"></span></h3>
						<h3><span id="jml"></span></h3>
			</div>
	</section>
</div>
<script>
	var table = document.getElementById("table-item"), sumJml = 0;
	for(var t = 1; t < table.rows.length; t++)
		{
			sumJml = sumJml + parseInt(table.rows[t].cells[6].innerHTML);
		}
		document.getElementById("jumlah").innerHTML = "Jumlah Laba = "+ sumJml;
</script>
<script>
	var table = document.getElementById("table-item"), sumJml = 0;
	for(var t = 1; t < table.rows.length; t++)
		{
			sumJml = sumJml + parseInt(table.rows[t].cells[5].innerHTML);
		}
		document.getElementById("jml").innerHTML = "Kotor = "+ sumJml;
</script>