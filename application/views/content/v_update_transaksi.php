<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div>

			</div>
		</div>
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info">
					<div class="card-body">
						<form id="form-ubah-transaksi" method="post" action="<?= site_url('Transaksi/proses_update') ?>"
							  role="form">
							<div class="row">
								<div class="col-sm-12">
								<div class="form-group">
                                        <label>Tanggal</label>
                                        <input type="date" class="form-control form-control-sm" id="tanggal" name="tanggal"
										value="<?= $transaksis->tanggal ?>" required>
                                    </div>
									<div class="form-group">
										<label>Jenis Topup</label>
										<input type="text" class="form-control form-control-sm" id="jenis"
											   name="jenis" value="<?= $transaksis->jenis ?>" placeholder="Enter ..."
											   required>
									</div>
									<div class="form-group">
										<label>No. Pelanggan</label>
										<input type="number" class="form-control form-control-sm" id="no_cust"
											   name="no_cust" value="<?= $transaksis->no_cust ?>" placeholder="Enter ..."
											   required>
									</div>
									<div class="form-group">
										<label>Harga Pokok</label>
										<input type="number" class="form-control form-control-sm" id="hrg_pokok"
											   name="hrg_pokok" value="<?= $transaksis->hrg_pokok ?>" placeholder="Enter ..."
											   required>
									</div>
									<div class="form-group">
										<label>Harga Jual</label>
										<input type="number" class="form-control form-control-sm" id="hrg_jual"
											   name="hrg_jual" value="<?= $transaksis->hrg_jual ?>" placeholder="Enter ..."
											   required>
									</div>
									<div class="form-group">
										<label for="">Cicilan</label>
										<select name="lunas_blmlns" class="form-control" id="lunas_blmlns">
											<option value="Lunas">Lunas</option>
											<option value="Belum Lunas">Belum Lunas</option>
										</select>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<button id="btn-save" class="btn btn-sm btn-success"><i class="fas fa-edit"></i>Ubah
								</button>
							</div>
							<input type="hidden" id="id_transaksi" name="id_transaksi"
								   value="<?= $transaksis->id_transaksi; ?>"/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--  -->
</div>