<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Saldo extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("ModelSaldo");
	}

	public function index()
	{
		$body = $this->ModelSaldo->getAll();
		$data = array(
			"header" => "Tabel Saldo",
			"page" => "saldo/v_list_saldo",
			"saldos" => $body
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function tambah()
    {
        $data = array(
            "header" => "Tambah Data Saldo",
            "page" => "saldo/v_add_saldo"
        );
        $this->load->view("layout/dashboard", $data);
    }

	public function proses_simpan(){
		$saldo = array(
			"id_saldo" => $this->input->post('id_saldo'),
			"nama" => $this->input->post('nama'),
			"saldo" => $this->input->post('saldo'),
		);
		$this->ModelSaldo->insert($saldo);
		redirect('Saldo');
	}

	public function update($id)
	{
		$listSaldo = $this->ModelSaldo->getByPrimaryKey($id);
		$data = array(
			"header" => "Ubah Data Saldo",
			"saldos" => $listSaldo,
			"page" => "saldo/v_update_saldo"
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function proses_update()
	{
		$id = $this->input->post("id_saldo", true);
		$saldos = array(
			"nama" => $this->input->post("nama", true),
			"saldo" => $this->input->post("saldo", true),
		);
		$this->ModelSaldo->update($id, $saldos);
		redirect("Saldo");
	}

	public function proses_hapus($id)
	{
		$this->ModelSaldo->delete($id);
		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success', 'Data Sukses dihapus');
		}
		redirect("Saldo");
	}


}
