<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("ModelTransaksi");
	}

	public function index()
	{
		$body = $this->ModelTransaksi->getAll();
		$data = array(
			"header" => "JASA TOPUP",
			"page" => "content/v_list_transaksi",
			"transaksis" => $body
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function tambah()
    {
        $data = array(
            "header" => "Tambah Transaksi",
            "page" => "content/v_add_transaksi"
        );
        $this->load->view("layout/dashboard", $data);
    }

	public function proses_simpan(){
		$hrgPokok = $this->input->post('hrg_pokok');
		$hrgJual = $this->input->post('hrg_jual');
		$laba = $hrgJual - $hrgPokok;
		$transaksi = array(
			"id_transaksi" => $this->input->post('id_transaksi'),
			"tanggal" => $this->input->post('tanggal'),
			"jenis" => $this->input->post('jenis'),
			"no_cust" => $this->input->post('no_cust'),
			"hrg_pokok" => $hrgPokok,
			"hrg_jual" => $hrgJual,
			"laba" => $laba,
			"lunas_blmlns" => $this->input->post('lunas_blmlns'),
		);
		$this->ModelTransaksi->insert($transaksi);
		redirect('Transaksi');
	}

	public function jml_laba()
	{
		$query = $this->db->query("SELECT SUM(laba) AS jumlah_laba FROM transaksi")->result();
		return $query;
	}

	public function update($id)
	{
		$listTransaksi = $this->ModelTransaksi->getByPrimaryKey($id);
		$data = array(
			"header" => "Ubah Transaksi",
			"transaksis" => $listTransaksi,
			"page" => "content/v_update_transaksi"
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function proses_update()
	{
		$id = $this->input->post("id_transaksi", true);
		$hrgPokok = $this->input->post('hrg_pokok', true);
		$hrgJual = $this->input->post('hrg_jual', true);
		$laba = $hrgJual - $hrgPokok;
		$transaksis = array(
			"tanggal" => $this->input->post("tanggal", true),
			"jenis" => $this->input->post("jenis", true),
			"no_cust" => $this->input->post("no_cust", true),
			"hrg_pokok" => $hrgPokok,
			"hrg_jual" => $hrgJual,
			"laba" => $laba,
			"lunas_blmlns" => $this->input->post('lunas_blmlns', true),
		);
		$this->ModelTransaksi->update($id, $transaksis);
		redirect("Transaksi");
	}

	public function proses_hapus($id)
	{
		$this->ModelTransaksi->delete($id);
		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success', 'Data Sukses dihapus');
		}
		redirect("Transaksi");
	}


}
